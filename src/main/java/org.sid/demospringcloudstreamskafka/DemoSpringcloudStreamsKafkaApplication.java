package org.sid.demospringcloudstreamskafka;

import org.sid.demospringcloudstreamskafka.entities.PageEvent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Date;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Supplier;

@SpringBootApplication
public class DemoSpringcloudStreamsKafkaApplication {

    public static void main (String[] args) {
        SpringApplication.run (DemoSpringcloudStreamsKafkaApplication.class, args);
    }

}
